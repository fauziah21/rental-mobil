<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('slug');
            $table->string('plat');
            $table->integer('price');
            $table->integer('total');
            $table->string('status')->nullable();
            $table->integer('stock');
            $table->string('name');
            $table->string('transmission');
            $table->text('image');
            $table->longText('about');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_packages');
    }
}
