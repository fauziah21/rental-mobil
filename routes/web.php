<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index')->name('home');
Route::get('/verification', 'HomeController@verification')->name('verification');
Route::get('/detail/{slug}', 'DetailController@index')->name('detail');

Route::post('/checkout/create/{id}', 'CheckoutController@create')->name('checkout-create')->middleware(['auth', 'verified']);
Route::get('/checkout/{id}', 'CheckoutController@index')->name('checkout')->middleware(['auth', 'verified']);
Route::get('/checkout/book/{id}', 'CheckoutController@book')->name('checkout-book')->middleware(['auth', 'verified']);
Route::post('/checkout/upload/{id}', 'CheckoutController@upload')->name('checkout-upload')->middleware(['auth', 'verified']);
Route::get('/checkout/confirm/{id}', 'CheckoutController@confirm')->name('checkout-confirm')->middleware(['auth', 'verified']);
Route::delete('/checkout/{id}', 'CheckoutController@delete')->name('checkout-delete')->middleware(['auth', 'verified']);
Route::get('/testimonial/{id}', 'CheckoutController@testimonial')->name('testimonial')->middleware(['auth', 'verified']);
Route::put('/testimonial/{id}', 'CheckoutController@updateTestimonial')->name('testimonial-update')->middleware(['auth', 'verified']);
Route::get('/myorder', 'CheckoutController@order')->name('myorder')->middleware(['auth', 'verified']);

Route::get('/about-us', 'FeatureController@about')->name('about-us');


Route::get('/profile', 'FeatureController@index')->name('profile-index');
Route::put('/profile/{id}', 'FeatureController@profile')->name('profile-update');


Route::prefix('admin')
    ->namespace('Admin')
    ->middleware(['auth', 'admin'])
    ->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::resource('rental-package', 'RentalPackageController');
        Route::resource('transaction', 'TransactionController');
        Route::resource('user', 'UserController');
        Route::get('/transfer', 'TransactionController@transfer')->name('transfer.index');
        Route::delete('/transfer/{transfer}', 'TransactionController@delete')->name('transfer.delete');
    });

Auth::routes(['verify' => true]);
