 <!-- navbar -->
 <div class="container">
     <nav class="row navbar navbar-expand-lg navbar-light bg-white">
         <a href="{{url('/')}}" class="navbar-brand">
             <img src="{{url('frontend/images/logo_satria@2x.png')}}" alt="logo satria" />
         </a>
         <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
             <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navb">
             <ul class="navbar-nav ml-auto mr-3">
                 <li class="nav-item mx-md-2">
                     <a href="{{route('home')}}" class="nav-link @yield('style-home')">Home</a>
                 </li>
                 @auth
                 <li class="nav-item mx-md-2">
                     <a href="{{route('profile-index')}}" class="nav-link @yield('style-profile')">Profile</a>
                 </li>
                 <li class="nav-item mx-md-2">
                     <a href="{{route('myorder')}}" class="nav-link @yield('style-myorder')">My Order</a>
                 </li>
                 @endauth
                 <li class="nav-item mx-md-2">
                     <a href="{{route('about-us')}}" class="nav-link @yield('style-about')">About Us</a>
                 </li>
             </ul>
             @guest
             <!-- mobile button -->
             <form class="form-inline d-sm-block d-md-none">
                 @csrf
                 <button class="btn btn-login my-2 my-sm-0" type="button" onclick="event.preventDefault(); location.href='{{url('login')}}';">
                     Masuk
                 </button>
             </form>

             <!-- desktop button -->
             <form class="form-inline my-2 my-lg-0 d-none d-md-block">
                 @csrf
                 <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" onclick="event.preventDefault(); location.href='{{url('login')}}';">
                     Masuk
                 </button>
             </form>
             @endguest

             @auth
             <!-- mobile button -->
             <form class="form-inline d-sm-block d-md-none" action="{{ url('logout')}}" method="POST">
                 @csrf
                 <button class="btn btn-login my-2 my-sm-0" type="submit">
                     Keluar
                 </button>
             </form>

             <!-- desktop button -->
             <form class="form-inline my-2 my-lg-0 d-none d-md-block" action="{{ url('logout')}}" method="POST">
                 @csrf
                 <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="submit">
                     Keluar
                 </button>
             </form>
             @endauth
         </div>
     </nav>
 </div>