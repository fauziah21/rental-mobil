
<link rel="stylesheet" href="{{url('frontend/libraries/bootstrap/css/bootstrap.css')}}" />

<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
    href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&family=Rajdhani:wght@300;400;500;600;700&display=swap"
    rel="stylesheet" />
<!--custom css-->
<link rel="stylesheet" href="{{url('frontend/styles/profile.css')}}" />