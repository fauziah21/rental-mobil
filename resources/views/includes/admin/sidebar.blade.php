<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-text mx-3">
            SATRIA Admin
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @yield('active-dashboard')">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item @yield('active-rental')">
        <a class="nav-link " href="{{route('rental-package.index')}}">
            <i class="fas fa-fw fa-car"></i>
            <span>Paket rental</span></a>
    </li>

    <li class="nav-item @yield('active-transfer')">
        <a class="nav-link" href="{{ route('transfer.index') }}">
            <i class="fas fa-fw fa-images"></i>
            <span>Upload Transfer</span></a>
    </li>

    <li class="nav-item @yield('active-transaksi')">
        <a class="nav-link" href="{{ route('transaction.index')}}">
            <i class="fas fa-fw fa-dollar-sign"></i>
            <span>Transaksi</span></a>
    </li>

    @if (Auth::user()->roles == 'SUPERADMIN')
    <li class="nav-item @yield('active-user')">
        <a class="nav-link" href="{{ route('user.index')}}">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span></a>
    </li>
    @endif


    <hr class="sidebar-divider">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>