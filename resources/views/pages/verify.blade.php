@extends('layouts.success')

@section('title', 'Verification')

@section('content')
<main>
    <section class="section-verify d-flex align-items-center">
        <div class="col text-center">
            <img src="{{url('frontend/images/mail.jpg')}}" alt="">
            <h1>Harap Cek Email Anda untuk Verifikasi Email</h1>
        </div>
    </section>
</main>
@endsection