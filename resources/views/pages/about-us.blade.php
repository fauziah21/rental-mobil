@extends('layouts.about-us')

@section('style-about', 'active')
@section('title','About Us')
@section('content')
<body >
        <section class="section-category" id="category">
            <div class="container-about">
              <div class="about-box">
                <article>
                    <h2 >About Us</h2>
                    <img src="frontend/images/testimonial3.png" alt="" />
                    <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
                        Cum eveniet non aperiam quis maiores vel quasi assumenda vero omnis reprehenderit facere fugit, 
                        iure temporibus laudantium iste. Velit alias quae ducimus?</p>
                </article>
                <table class="informasi-pembuat">
                    <tr>
                        <th width="50%" class="text-left">Nama</th>
                        <td width="50%" class="text-left">Nur sakti</td>
                    </tr>
                    <tr>
                        <th width="50%" class="text-left">Kelas</th>
                        <td width="50%" class="text-left">4IA04</td>
                    </tr>
                    <tr>
                        <th width="50%" class="text-left">Npm</th>
                        <td width="50%" class="text-left">7362526</td>
                    </tr>
                    <tr>
                        <th width="50%" class="text-left">Dosen</th>
                        <td width="50%" class="text-left">Miftahul Jannah</td>
                    </tr>

                </table>  
                </div>
            </div>
          </section>
@endsection
@push('addon-script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/610a3776649e0a0a5ccf6e70/1fc7uha62';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endpush