@extends('layouts.checkout')

@section('title', 'Checkout')

@section('content')
<main>
    <section class="section-details-header"></section>
    <section class="section-details-content">
        <div class="container">
            <div class="row">
                <div class="col p-0">
                    <div class="nav">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                Sewa Mobil
                            </li>
                            <li class="breadcrumb-item">
                                Details
                            </li>
                            <li class="breadcrumb-item active">
                                Checkout
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <section class="section-checkout">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-checkout">
                            <h1 class="text-center">Informasi Penyewaan</h1>
                            <table class="informasi-penyewaan">
                                <tr>
                                    <th width="50%">Nama</th>
                                    <td width="50%" class="text-left">{{ $item->rental_package->name}}</td>
                                </tr>
                                <tr>
                                    <th width="50%">Jenis</th>
                                    <td width="50%" class="text-left">{{ $item->rental_package->type}}</td>
                                </tr>
                                <tr>
                                    <th width="50%">Transmisi</th>
                                    <td width="50%" class="text-left">{{ $item->rental_package->transmission}}</td>
                                </tr>
                                <tr>
                                    <th width="50%">Tanggal Mulai</th>
                                    <td width="50%" class="text-left">
                                        <?php
                                        $mulai = date("F j, Y", strtotime($item->start_date));
                                        echo $mulai;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Tanggal Pengembalian</th>
                                    <td width="50%" class="text-left">
                                        <?php
                                        $selesai = date("F j, Y", strtotime($item->end_date));
                                        echo $selesai;
                                        ?></td>
                                </tr>
                                <tr>
                                    <th width="50%">Lokasi Penjemputan</th>
                                    @if ($item->location == null)
                                    <td width="50%" class="text-left">Garasi</td>
                                    @else
                                    <td width="50%" class="text-left">{{ $item->location}}</td>
                                    @endif

                                </tr>
                                <tr>
                                    <th width="50%">Waktu Penjemputan</th>
                                    <td width="50%" class="text-left">{{ \Carbon\Carbon::create($item->pick_up_time)->format('H:i')}}</td>
                                </tr>
                                <tr>
                                    <th width="50%">Total Harga</th>
                                    <td width="50%" class="text-left">Rp {{ $item->transaction_total}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="informasi-pembayaran">
                            <div class="informasi-pembayaran-content text-center">
                                <h2>Selesaikan Pembayaran Sebelum</h2>


                                <p class="tanggal">{{ \Carbon\Carbon::create($item->deadline_day)->format('F j, Y')}} {{ \Carbon\Carbon::create($item->deadline_time)->format('H:i')}}</p>
                            </div>
                            <hr>
                            <h2 class="text-center mb-3">Instruksi Pembayaran</h2>
                            <div class="bank">
                                <div class="bank-item pb-3">
                                    <img src="{{url('frontend/images/bca.png')}}" alt="bank bca" class="bank-image">
                                    <div class="description">
                                        <h3>a/n PT Satria</h3>
                                        <p>
                                            0877 2734 6848
                                            <br>
                                            Bank Central Asia
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="bank-item pb-3">
                                    <img src="{{url('frontend/images/mandiri.png')}}" alt="bank mandiri" class="bank-image">
                                    <div class="description">
                                        <h3>a/n PT Satria</h3>
                                        <p>
                                            0947 7693 5347
                                            <br>
                                            Bank Mandiri
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="join-container">
                                <a href="{{route('checkout-confirm', $item->id)}}" class="btn btn-block btn-join-now mt-3 py-2">I Have Made
                                    Payment</a>
                            </div>
                        </div>
                        <div class="text-center -mt-3">
                            <form action="{{route('checkout-delete', $item->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn-cancel mt-2">Cancel Booking</button>
                            </form>

                        </div>

                        <div class="note">
                            <p>
                                Catatan
                                <ul>
                                    <li>
                                        <strong>Simpan bukti pembayaran</strong> yang sewaktu-waktu diperlukan jika
                                        terjadi kendala
                                        transaksi
                                    </li>
                                    <li>
                                        Pesanan <strong style="color: red;">otomatis dibatalkan</strong> apabila
                                        tidak melakukan
                                        pembayaran dari
                                        waktu
                                        yang
                                        ditentukan
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>


            </section>
        </div>
    </section>
</main>
@endsection