@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail Transaksi {{$item->user->name}}</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>
                {{$error}}
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow">
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <td>{{ $item->id}}</td>
                </tr>
                <tr>
                    <th>Mobil</th>
                    <td>{{ $item->rental_package->name}}</td>
                </tr>
                <tr>
                    <th>Penyewa</th>
                    <td>{{ $item->user->name}}</td>
                </tr>
                <tr>
                    <th>Deadline</th>
                    <td>{{ $item->deadline_day}}</td>
                </tr>
                <tr>
                    <th>Waktu Deadline</th>
                    <td>{{ $item->deadline_time}}</td>
                </tr>
                <tr>
                    <th>Total Transaksi</th>
                    <td>Rp {{ $item->transaction_total}}</td>
                </tr>
                <tr>
                    <th>Status Transaksi</th>
                    <td>{{$item->transaction_status}}</td>
                </tr>
                <tr>
                    <th>Start Date</th>
                    <td>{{$item->start_date}}</td>
                </tr>
                <tr>
                    <th>End Date</th>
                    <td>{{$item->end_date}}</td>
                </tr>
                <tr>
                    <th>Pick Up Location</th>
                    <td>{{$item->pick_up_location}}</td>
                </tr>
                <tr>
                    <th>Pick Up Time</th>
                    <td>{{$item->pick_up_time}}</td>
                </tr>
                <tr>
                    <th>Testimonial</th>
                    <td>{{$item->testimonial}}</td>
                </tr>
            </table>
        </div>
    </div>

</div>
@endsection