@extends('layouts.admin')

@section('active-transfer', 'active')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Transaksi</h1>
    </div>

    <div class="row">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" colspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>ID Transaksi</th>
                            <th>User</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($items as $key => $item )
                        <tr>
                            <td>{{$items->firstItem()+ $key }}</td>
                            <td>{{ $item->id}}</td>
                            <td>{{ $item->transaction->id }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>
                                <img src="/storage/{{$item->image}}" alt="">
                            </td>
                            <td>
                                <form action="{{route('transfer.delete', $item->id)}}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">No Data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="float-sm-left mt-1">
                    Showing
                    {{$items->firstItem()}}
                    to
                    {{$items->lastitem()}}
                    of
                    {{$items->total()}}
                    entries
                </div>
                <div class="float-sm-right mt-2">
                    {{$items->links()}}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection