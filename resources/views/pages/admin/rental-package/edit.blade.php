@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Rental Mobil {{$item->name}}</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>
                {{$error}}
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow">
        <div class="card-body">
            <form action="{{route('rental-package.update', $item->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{$item->name}}">
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <input type="text" class="form-control" name="type" placeholder="Type" value="{{$item->type}}">
                </div>
                <div class="form-group">
                    <label for="transmission">Transmission</label>
                    <input type="text" class="form-control" name="transmission" placeholder="Transmission" value="{{$item->transmission}}">
                </div>
                <div class="form-group">
                    <label for="plat">Plat</label>
                    <input type="text" class="form-control" name="plat" placeholder="Plat Mobil" value="{{$item->plat}}">
                </div>

                <div class="form-group">
                    <label for="total">Total</label>
                    <input type="number" min="0" class="form-control" name="total" placeholder="Total Mobil" value="{{$item->total}}">
                </div>
                <div class="form-group">
                    <label for="stock">Stock</label>
                    <input type="number" min="0" class="form-control" name="stock" placeholder="Stock" value="{{$item->stock}}">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control" name="image" placeholder="Image" value="{{$item->image}}">
                </div>
                <div class="form-group">
                    <label for="about">About</label>
                    <textarea name="about" rows="10" class="d-block w-100 form-control">{{$item->about}}</textarea>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" min="0" class="form-control" name="price" placeholder="Price" value="{{$item->price}}">
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    Update
                </button>
                <a href="{{route('rental-package.index')}}" class="btn btn-danger btn-block">Cancel</a>
            </form>
        </div>
    </div>

</div>
@endsection