@extends('layouts.admin')

@section('active-rental', 'active')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Paket Rental Mobil</h1>
        <a href="{{route('rental-package.create')}}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Paket Rental Mobil
        </a>
    </div>

    <div class="row">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" colspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Transmission</th>
                            <th>Plat</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Stock</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($items as $key => $item )
                        <tr>
                            <td>{{$items->firstItem() + $key }}</td>
                            <td>{{ $item->id}}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->transmission }}</td>
                            <td>{{ $item->plat }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->total }}</td>
                            <td>{{ $item->stock }}</td>
                            <td>{{ $item->status}}</td>
                            <td>
                                <a href="{{route('rental-package.edit', $item->id)}}" class="btn btn-info">
                                    <i class="fa fa-pencil-alt"></i>
                                </a>
                                <form action="{{route('rental-package.destroy', $item->id)}}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10" class="text-center">No Data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="float-sm-left mt-1">
                    Showing
                    {{$items->firstItem()}}
                    to
                    {{$items->lastitem()}}
                    of
                    {{$items->total()}}
                    entries
                </div>
                <div class="float-sm-right mt-2">
                    {{$items->links()}}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection