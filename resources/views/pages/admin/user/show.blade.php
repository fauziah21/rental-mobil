@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail User {{$item->name}}</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>
                {{$error}}
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow">
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <td>{{ $item->id}}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{{ $item->name}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $item->email}}</td>
                </tr>
                <tr>
                    <th>Username</th>
                    <td>{{ $item->username}}</td>
                </tr>
                <tr>
                    <th>Image</th>
                    <td>
                        @if ($item->image == null)
                        <img src="https://ui-avatars.com/api/?name={{$item->name}}" class="rounded-circle" style="width: 50px; height:50px" />
                        @else
                        <img src="/storage/{{$item->image}}" style="width: 50px; height: 50px">
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>KTP</th>
                    <td>{{$item->ktp}}</td>
                </tr>
                <tr>
                    <th>SIM</th>
                    <td>{{$item->sim}}</td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{$item->address}}</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>{{$item->phone}}</td>
                </tr>
                <tr>
                    <th>Role</th>
                    <td>{{$item->roles}}</td>
                </tr>
            </table>
        </div>
    </div>

</div>
@endsection