@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit User {{$item->name}} Data</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>
                {{$error}}
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow">
        <div class="card-body">
            <form action="{{route('user.update', $item->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="roles">Role</label>
                    <select name="roles" required class="form-control">
                        <option value="{{$item->roles}}">
                            Jangan Ubah ({{$item->roles}})
                        </option>
                        <option value="USER">User</option>
                        <option value="ADMIN">Admin</option>
                        <option value="SUPERADMIN">Super Admin</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    Update
                </button>
                <a href="{{route('user.index')}}" class="btn btn-danger btn-block">Cancel</a>
            </form>
        </div>
    </div>

</div>
@endsection