@extends('layouts.admin')

@section('active-user', 'active')
@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Users</h1>
    </div>

    <div class="row">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" colspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($items as $key => $item )
                        <tr>
                            <td>{{$items->firstItem() + $key }}</td>
                            <td>{{ $item->id}}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->username }}</td>
                            <td>{{ $item->roles }}</td>
                            <td>
                                <a href="{{route('user.show', $item->id)}}" class="btn btn-primary">
                                    <i class="fa fa-eye "></i>
                                </a>
                                <a href="{{route('user.edit', $item->id)}}" class="btn btn-info">
                                    <i class="fa fa-pencil-alt"></i>
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6" class="text-center">No Data</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="float-sm-left mt-1">
                    Showing
                    {{$items->firstItem()}}
                    to
                    {{$items->lastitem()}}
                    of
                    {{$items->total()}}
                    entries
                </div>
                <div class="float-sm-right mt-2">
                    {{$items->links()}}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection