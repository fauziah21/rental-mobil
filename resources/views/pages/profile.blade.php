@extends('layouts.profile')

@section('style-profile', 'active')
@section('title','Profile')
@section('content')
<header>

    <body>
        <div class="container-profile">
            <div class="profile-box">
                <div class="right">
                    <div class="title">Profile</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>
                                {{$error}}
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{route('profile-update', Auth::user()->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <class class="user-details">
                            <div class="img">
                                @if (Auth::user()->image == null)
                                <img src="https://ui-avatars.com/api/?name={{Auth::user()->name}}" class="rounded-circle" style="width: 160px; height:160px" />
                                @else
                                <img src="/storage/{{Auth::user()->image}}" class="rounded-circle" style="width: 160px; height:160px" />
                                @endif

                                <input type="file" id="inputpic" name="image">
                            </div>
                            <div class="img">
                                <label for="inputpic">Edit Picture</label>
                            </div>
                            <div class="input-box">
                                <span class="details">Name</span>
                                <input type="text" name="name" placeholder="" value="{{ Auth::user()->name}}">
                            </div>
                            <div class="input-box">
                                <span class="details">Email</span>
                                <input type="text" name="email" placeholder="" value="{{ Auth::user()->email}}" disabled>
                            </div>
                            <div class="input-box">
                                <span class="details">Username</span>
                                <input type="text" name="username" placeholder="" value="{{ Auth::user()->username}}">
                            </div>
                            <div class="input-box">
                                <span class="details">KTP</span>
                                <input type="text" name="ktp" placeholder="" value="{{ Auth::user()->ktp}}">
                            </div>
                            <div class="input-box">
                                <span class="details">Address</span>
                                <input type="text" name="address" placeholder="" value="{{ Auth::user()->address}}">
                            </div>
                            <div class="input-box">
                                <span class="details">Phone</span>
                                <input type="text" name="phone" placeholder="" value="{{ Auth::user()->phone}}">
                            </div>
                            <div class="input-box">
                                <span class="details">SIM</span>
                                <input type="text" name="sim" placeholder="" value="{{ Auth::user()->sim}}">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">
                                Update
                            </button>
                        </class>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </body>
    </main>

</header>
@endsection

@push('addon-script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/610a3776649e0a0a5ccf6e70/1fc7uha62';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endpush