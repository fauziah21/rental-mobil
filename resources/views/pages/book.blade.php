@extends('layouts.success')

@section('title', 'Book')

@section('content')
<main>
    <section class="section-book d-flex align-items-center">
        <div class="col text-center">
            <img src="{{url('frontend/images/waiting.jpg')}}" alt="">
            <h1>
                Harap Tunggu
            </h1>
            <p class="text-muted">
                Pesanan Kamu Sedang Dicek Oleh Admin
            </p>
            <a href="{{url('/')}}" class="btn btn-home mt-3 px-5">Home</a>
        </div>
    </section>
</main>
@endsection