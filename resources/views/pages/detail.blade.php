@extends('layouts.app')

@section('title')
Detail Rental
@endsection

@section('content')
<main>
    <section class="section-details-header"></section>
    <section class="section-details-content">
        <div class="container">
            <div class="row">
                <div class="col p-0">
                    <div class="nav">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                Sewa Mobil
                            </li>
                            <li class="breadcrumb-item active">
                                Details
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 pl-lg-0">
                    <div class="card card-details">
                        <h1>{{$item->name}}</h1>
                        <div class="gallery">
                            <img src="/storage/{{ $item->image}}" alt="{{$item->name}}">
                        </div>
                        <h2>Informasi Mobil</h2>
                        <p>
                            {!! $item->about !!}
                        </p>
                        <br>
                        <p class="ilustrasi">
                            * Gambar unit di aplikasi hanya ilustrasi, untuk real picture bisa hubungi customer
                            service kami.
                        </p>
                        <hr>
                        <div class="row features">
                            <div class="col-md-2">
                                <i class="fas fa-car fa-lg"></i>
                                <div class="description">
                                    <h3>Nomor Polisi</h3>
                                    <p>{{$item->plat}}</p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <i class="fas fa-cog"></i>
                                <div class="description">
                                    <h3>Transmisi</h3>
                                    <p>{{ $item->transmission}}</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <i class="fas fa-dollar-sign"></i>
                                <div class="description">
                                    <h3>Harga</h3>
                                    <p>Rp {{ $item->price}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card card-details card-right">
                        <h2>Detail Sewa Mobil</h2>
                        <hr>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>
                                    {{$error}}
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form action="{{route('checkout-create', $item->id)}}" method="POST" class="form-row">
                            @csrf
                            <div class="form-group col-md-6">
                                <label for="start_date">Tanggal Mulai</label>
                                <input type="date" name="start_date" min="{{date('Y-m-d')}}" class="form-control txtDate">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="end_date">Tanggal Selesai</label>
                                <input type="date" name="end_date" min="{{date('Y-m-d')}}" class="form-control txtDate">
                            </div>
                            <div class="form-group col-12">
                                <label for="pick_up_location">Lokasi Penjemputan</label> <br>
                                <input type="radio" onclick="javascript:yesnoCheck();" name="pick_up_location" value="garasi" id="garasi"> Garasi
                                <input type="radio" onclick="javascript:yesnoCheck();" name="pick_up_location" value="antar" id="antar" class="ml-2"> Diantar
                                <div id="ifDiantar" style="display:none">
                                    <input type="text" class="form-control" name="location" placeholder="Masukkan Lokasi">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="pick_up_time">Waktu Penjemputan</label>
                                <input type="time" name="pick_up_time" id="pick_up_time" class="form-control">
                            </div>
                            @auth
                            <button type="submit" class="btn btn-pesan-sekarang btn-lg btn-block">
                                Pesan Sekarang
                            </button>
                            @endauth
                            @guest
                            <a href="{{route('login')}}" class="btn btn-pesan-sekarang btn-lg btn-block">
                                Login atau Register untuk Pesan
                            </a>
                            @endguest
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push('addon-script')
<script type="text/javascript">
    $(function() {
        var dtToday = new Date();

        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10) month = '0' + month.toString();
        if (day < 10) day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        // alert(maxDate);
        $('.txtDate').attr('min', maxDate);
    });

    function yesnoCheck() {
        if (document.getElementById('antar').checked) {
            document.getElementById('ifDiantar').style.display = 'block';
        } else document.getElementById('ifDiantar').style.display = 'none';

    }
</script>

@endpush