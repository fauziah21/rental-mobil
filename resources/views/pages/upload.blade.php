@extends('layouts.success')

@section('title', 'Upload')

@section('content')
<main>
    <section class="section-upload d-flex align-items-center">
        <div class="col text-center">
            <img src="{{url('frontend/images/upload.jpg')}}" alt="">
            <div class="card card-upload shadow p-3 mb-5 bg-white rounded">
                <div class="card-body">
                    <h1>Silahkan Upload Bukti Pembayaran Disini</h1>
                    <form action="{{route('checkout-upload', $transaction->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="file" class="form-control" name="image" placeholder="Image">
                        </div>
                        <button type="submit" class="btn btn-upload mt-5 px-5">
                            Save
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection