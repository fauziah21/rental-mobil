@extends('layouts.app')

@section('title', 'SATRIA')

@section('style-home', 'active')

@section('style', ' ')

@section('content')
<!-- header -->
<header class="text-justify">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>
                    Rental Sewa Mobil
                    <br />
                    Satria
                </h1>
                <p class="mt-3">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Pellentesque ac ligula eget magna commodo vulputate. Mauris
                    faucibus, orci id aliquam faucibus, felis mi gravida quam, in
                    bibendum massa risus iaculis eros. Duis vel lacus nisl. Donec
                    consectetur mauris eu finibus posuere.
                </p>
                <a href="#category" class="btn btn-get-started px-4 mt-4">
                    Get Started
                </a>
            </div>
            <div class="col-md-6">
                <img src="{{url('frontend/images/car.svg')}}" alt="" />
            </div>
        </div>
    </div>
</header>

<main>
    <section class="section-category" id="category">
        <div class="container">
            <div class="row">
                <div class="col text-center section-category-heading">
                    <h2>Daftar Armada Kami</h2>
                    <p>
                        Dibawah ini berbagai armada yang kami miliki, silahkan pilih
                        <br />
                        armada sesuai kebutuhan anda.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="section-category-content" id="sectionCategory">
        <div class="container">
            <div class="section-category-car row justify-content-center">
                @foreach ($items as $item)
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="card card-car ">
                        <div class="car-content">
                            <img src="/storage/{{$item->image}}" class="mb-4" />
                            <h3 class="mb-4">{{$item->name}}</h3>
                            <h4>Rp {{$item->price}}</h4>
                            <ul>
                                <li>
                                    <i class=" fas fa-cog"></i>
                                    <span>Transmisi: {{$item->transmission}}</span></a>
                                </li>
                                <li>
                                    <i class="fas fa-car"></i>
                                    <span>Jenis: {{$item->type}}</span></a>
                                </li>
                            </ul>
                            @if ($item->stock > 0)
                            <a href="{{route('detail', $item->slug)}}" class="btn btn-block btn-pesan">Pesan Sekarang</a>
                            @else
                            <a href="#" class="btn btn-block btn-pesan disabled">Habis</a>
                            @endif

                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <section class="section-testimonial-heading" id="testimonialHeading">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Apa Kata Mereka</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="section-testimonial-content" id="testimonial-content">
        <div class="container">
            <div class="section-popular-car row justify-content-center">
                <div class="col-md-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/dena.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Dena</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan sewa mobil
                                                    <br>
                                                    untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil avanza
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/muthi.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Muthi</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan sewa
                                                    <br>
                                                    mobil untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil rush
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/syeima.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Syeima</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan
                                                    <br> sewa mobil
                                                    untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil rush
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item ">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/ilham.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Ilham</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan sewa mobil
                                                    <br>
                                                    untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil avanza
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/muthi.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Muthi</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan sewa
                                                    <br>
                                                    mobil untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil rush
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <div class="card card-testimonial text-center">
                                            <div class="testimonial-content">
                                                <img src="frontend/images/nayya.png" alt="user" class="mb-4 rounded-circle">
                                                <h3 class="mb-4">Nayya</h3>
                                                <p class="testimonial">
                                                    Terimakasih sudah menyediakan sewa mobil
                                                    <br>
                                                    untuk kebutuhan mendesak
                                                </p>
                                            </div>
                                            <hr class="mt-auto">
                                            <p class="rent-car mt-2">
                                                Menyewa mobil rush
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 text-center">
                <a href="#" class="btn btn-perlu-bantuan px-4 mt-4 mx-1">Perlu Bantuan</a>
                <a href="#" class="btn btn-get-started px-4 mt-4 mx-1">
                    Get Started
                </a>
            </div>
        </div>
    </section>
</main>
@endsection
@push('addon-script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/610a3776649e0a0a5ccf6e70/1fc7uha62';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endpush