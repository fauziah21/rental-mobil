@extends('layouts.myorder')

@section('title','My Order')
@section('style-myorder','active')
@section('content')
<main>
  <div id="reservation">
    <section class="header-reservation">
      <div class="container-reservation">
        <div class="header-menu">
          <ul class="nav nav-tabs">
            <li class="col-sm-4 col-xs-4">
              <a href="#inprogress" aria-controls="inprogress" role="tab" data-toggle="tab" aria-expanded="true">In Cart</a>
            </li>
            <li class="col-sm-4 col-xs-4">
              <a href="#pending" aria-controls="pending" role="tab" data-toggle="tab" aria-expanded="false">In Progress</a>
            </li>
            <li class="col-sm-4 col-xs-4">
              <a href="#complete" aria-controls="complete" role="tab" data-toggle="tab" aria-expanded="false">Completed</a>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <section class="content-reservation">
      <div class="container-reservation">
        <div class="tab-content" style="position: static; zoom: 1;">
          <div id="inprogress" role="tabpanel" class="tab-pane">
            <div class="text-center f-grey">
              @forelse ($items as $item )
              @if ($item->transaction_status == 'IN_CART' && Auth::user()->id == $item->users_id)
              <div class="card-data">
                <div class="card-data__img">
                  <img src="/storage/{{$item->rental_package->image}}">
                </div>
                <div class="card-data__info">
                  <div div class="card-data__date">
                    <span>{{ \Carbon\Carbon::create($item->deadline_day)->format('F j, Y')}}</span>
                  </div>
                  <h5 class="card-data__text">{{ $item->rental_package->name }}</h5>
                  <h7 class="card-data__status ">Check Detail Transaction Here</h7>
                  <a href="{{route('checkout', $item->id)}}" class="card-data__detail">Detail</a>
                </div>
              </div>
              @endif
              @empty
              <div class="text-center f-grey">
                <img src="{{url('frontend/images/car.svg')}}" height="200px" width="200px">
                <h4>No Order In Progress</h4>
                <p>You dont have any order yet</p>
              </div>
              @endforelse
            </div>
          </div>
          <div id="complete" role="tabpanel" class="tab-pane active">
            <div class="text-center f-grey">
              @forelse ($items as $item )
              @if ($item->transaction_status == 'SUCCESS' && Auth::user()->id == $item->users_id)
              <div class="card-data">
                <div class="card-data__img">
                  <img src="/storage/{{$item->rental_package->image}}">
                </div>
                <div class="card-data__info">
                  <div div class="card-data__date">
                    <span>{{ \Carbon\Carbon::create($item->start_date)->format('F j, Y')}}</span>
                  </div>
                  <h5 class="card-data__text">{{ $item->rental_package->name }}</h5>
                  <p class="card-data__status">Order Completed</p>
                  @if ($item->testimonial == null)
                  <a href="{{ route('testimonial', $item->id)}}" class="card-data__detail">Review</a>
                  @else
                  <a href="#" class="card-data__detail disabled">Reviewed</a>
                  @endif

                </div>
              </div>
              @endif
              @empty
              <div class="text-center f-grey">
                <img src="frontend/images/car.svg" height="200px" width="200px">
                <h4>No Order Completed</h4>
                <p>You dont have any order yet</p>
              </div>
              @endforelse
            </div>
          </div>
          <!-- pending -->
          <div id="pending" role="tabpanel" class="tab-pane ">
            <div class="text-center f-grey">
              @forelse ($items as $item )
              @if ($item->transaction_status == 'PENDING' && Auth::user()->id == $item->users_id)
              <div class="card-data">
                <div class="card-data__img">
                  <img src="/storage/{{$item->rental_package->image}}">
                </div>
                <div class="card-data__info">
                  <div div class="card-data__date">
                    <span>{{ \Carbon\Carbon::create($item->start_date)->format('F j, Y')}}</span>
                  </div>
                  <h5 class="card-data__text">{{ $item->rental_package->name }}</h5>
                  <p class="card-data__status">Processing</p>
                </div>
              </div>
              @endif
              @empty
              <div class="text-center f-grey">
                <img src="frontend/images/car.svg" height="200px" width="200px">
                <h4>No Order Completed</h4>
                <p>You dont have any order yet</p>
              </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</main>
@endsection
@push('addon-script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
  var Tawk_API = Tawk_API || {},
    Tawk_LoadStart = new Date();
  (function() {
    var s1 = document.createElement("script"),
      s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/610a3776649e0a0a5ccf6e70/1fc7uha62';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
  })();
</script>
<!--End of Tawk.to Script-->
@endpush