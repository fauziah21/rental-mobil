@extends('layouts.Testimonial')

@section('title','Testimonial')
@section('content')

<body>
    <div class="container-about">
        <div class="about-box px-4">
            <article>
                <h2>Testimonial</h2>
            </article>
            @if(Auth::user()->image == null)
            <img src="https://ui-avatars.com/api/?name={{Auth::user()->name}}">
            @else
            <img src="/storage/{{ $item->user->image}}" alt="" />
            @endif
            <article>
                <h3 class="username">Safira</h3>
                <p>Ulasan bersifat publik serta menyertakan informasi terkait penyewaan.</p>
            </article>
            <form action="{{route('testimonial-update', $item->id)}}" method="POST">
                @csrf
                @method('put')
                <textarea name="testimonial" placeholder="Deskripsikan Pengalaman Anda" class="tekstesti"></textarea>
                <button type="submit" class="btn-Post">Posting</button>
            </form>
        </div>
    </div>
</body>
</header>
@endsection
@push('addon-script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/610a3776649e0a0a5ccf6e70/1fc7uha62';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
@endpush