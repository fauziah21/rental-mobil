<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'users_id', 'rental_packages_id', 'transaction_total', 'transaction_status', 'deadline_day', 'deadline_time', 'start_date', 'end_date', 'pick_up_location', 'pick_up_time', 'testimonial', 'location'
    ];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function rental_package()
    {
        return $this->belongsTo(RentalPackage::class, 'rental_packages_id', 'id');
    }
}
