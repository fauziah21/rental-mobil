<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentalPackage extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type', 'slug', 'plat', 'price', 'total', 'status', 'stock', 'name', 'transmission', 'image', 'about'
    ];

    protected $hidden = [];
}
