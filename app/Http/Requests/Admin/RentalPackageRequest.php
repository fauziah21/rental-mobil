<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RentalPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|max:50',
            'plat' => 'required|max:20',
            'price' => 'required|integer',
            'total' => 'required|integer',
            'stock' => 'required|integer',
            'name' => 'required|max:50',
            'transmission' => 'required|max:50',
            'image' => 'required|mimes:jpeg,png,jpg,svg',
            'about' => 'required|max:1000'

        ];
    }
}
