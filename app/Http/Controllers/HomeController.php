<?php

namespace App\Http\Controllers;

use App\RentalPackage;
use App\Transaction;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = RentalPackage::get();

        $testimonial = Transaction::with('user', 'rental_package')
            ->where('id', 1)
            ->count();
        return view('pages.home', compact('items', 'testimonial'));
    }

    public function verification()
    {
        return view('pages.verify');
    }
}
