<?php

namespace App\Http\Controllers;

use App\RentalPackage;
use App\Transaction;
use App\Transfer;

use Carbon\carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function index(Request $request, $id)
    {
        $item = Transaction::with('user', 'rental_package')->findOrFail($id);
        // dd($item);
        return view('pages.checkout', compact('item'));
    }

    public function order()
    {
        $items = Transaction::with('user', 'rental_package')->get();
        return view('pages.myorder', compact('items'));
    }

    public function delete($id)
    {
        $item = Transaction::findOrFail($id);
        $item->delete();

        return redirect()->route('home');
    }

    public function create(Request $request, $id)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'pick_up_location' => 'required',
            'pick_up_time' => 'required'
        ]);

        // dd($request);
        $rental_package = RentalPackage::findOrFail($id);

        $amount = Carbon::parse($request['start_date'])->diffInDays($request['end_date']);
        // dd($amount);
        if ($amount == 0) {
            $transaction = Transaction::create([
                'rental_packages_id' => $id,
                'users_id' => Auth::user()->id,
                'transaction_total' => $rental_package->price,
                'transaction_status' => 'IN_CART',
                'deadline_day' => Carbon::now()->addDay(),
                'deadline_time' => Carbon::now()->addHours(24),
                'start_date' => $request["start_date"],
                'end_date' => $request["end_date"],
                'pick_up_location' => $request["pick_up_location"],
                'location' => $request["location"],
                'pick_up_time' => $request["pick_up_time"]
            ]);
        } else {
            $transaction = Transaction::create([
                'rental_packages_id' => $id,
                'users_id' => Auth::user()->id,
                'transaction_total' => Carbon::parse($request['start_date'])->diffInDays($request['end_date']) * $rental_package->price,
                'transaction_status' => 'IN_CART',
                'deadline_day' => Carbon::now()->addDay(),
                'deadline_time' => Carbon::now()->addHours(24),
                'start_date' => $request["start_date"],
                'end_date' => $request["end_date"],
                'pick_up_location' => $request["pick_up_location"],
                'location' => $request["location"],
                'pick_up_time' => $request["pick_up_time"]
            ]);
        }

        return redirect()->route('checkout', $transaction->id);
    }

    public function confirm(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        return view('pages.upload', compact('transaction'));
    }

    public function upload(Request $request, $id)
    {
        // dd($request);
        $transaction = Transaction::findOrFail($id);
        // dd($transaction);

        $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,svg',
        ]);

        $data = $request->all();
        // dd($data);

        $imgName = $request->file('image')->getClientOriginalName();
        $data['image'] = $request->file('image')->storeAs(
            'assets/gallery/transfer',
            $imgName,
            'public'
        );

        $data = Transfer::create([
            'users_id' => Auth::user()->id,
            'transactions_id' => $transaction->id,
            'image' => $data['image']
        ]);

        $transaction->transaction_status = 'PENDING';

        $transaction->save();
        return redirect()->route('checkout-book', $transaction->id);
    }

    public function book()
    {
        return view('pages.book');
    }

    public function testimonial(Request $request, $id)
    {
        $item = Transaction::with('user', 'rental_package')->findOrFail($id);
        return view('pages.Testimonial', compact('item'));
    }

    public function updateTestimonial(Request $request, $id)
    {
        $request->validate([
            'testimonial' => 'string|required|max:1000'
        ]);

        $data = $request->all();

        $item = Transaction::with('user', 'rental_package')->findOrFail($id);

        $item->update($data);

        return redirect()->route('home');
    }
}
