<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RentalPackageRequest;
use App\RentalPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RentalPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = RentalPackage::paginate(5);
        return view('pages.admin.rental-package.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.rental-package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RentalPackageRequest $request)
    {
        $data = $request->all();
        // dd($data);

        $data['slug'] = Str::slug($request->name);
        $data['status'] = ($data['stock'] > 0) ? "Available" : "Unavailable";
        $imgName = $request->file('image')->getClientOriginalName();
        // dd($imgName);
        $data['image'] = $request->file('image')->storeAs(
            'assets/gallery',
            $imgName,
            'public'
        );

        RentalPackage::create($data);

        return redirect()->route('rental-package.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = RentalPackage::findOrFail($id);

        return view('pages.admin.rental-package.edit', compact(('item')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type' => 'max:50',
            'plat' => 'max:20',
            'price' => 'integer',
            'total' => 'integer',
            'stock' => 'integer',
            'name' => 'max:50',
            'transmission' => 'max:50',
            'image' => 'mimes:jpeg,png,jpg,gif,svg',
            'about' => 'max:1000'
        ]);

        $data = $request->all();
        // dd($data);

        $data['slug'] = Str::slug($request->name);

        if ($request->hasFile('image')) {
            $imgName = $request->file('image')->getClientOriginalName();
            $data['image'] = $request->file('image')->storeAs(
                'assets/gallery',
                $imgName,
                'public'
            );
        }

        if ($data['stock'] == 0) {
            $data['status'] = 'Unavailable';
        }


        $item = RentalPackage::findOrFail($id);

        $item->update($data);

        return redirect()->route('rental-package.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = RentalPackage::findOrFail($id);
        $item->delete();

        return redirect()->route('rental-package.index');
    }
}
