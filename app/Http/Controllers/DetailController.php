<?php

namespace App\Http\Controllers;

use App\RentalPackage;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function index(Request $request, $slug)
    {
        $item = RentalPackage::where('slug', $slug)->firstOrFail();

        return view('pages.detail', compact('item'));
    }
}
