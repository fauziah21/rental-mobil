<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class FeatureController extends Controller
{
    public function about(Request $request)
    {
        return view('pages.about-us');
    }
    public function myorder(Request $request)
    {
        return view('pages.myorder');
    }
    public function profile(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'name' => ['required', 'string', 'max:50'],
            'username' => ['string', 'max:20'],
            'ktp' => ['string', 'max:20'],
            'address' => ['string', 'max:100'],
            'phone' => ['string', 'max:20'],
            'sim' => ['string', 'max:20'],
            'image' => 'mimes:jpeg,jpg'
        ]);

        $data = $request->all();

        if ($request->hasFile('image')) {
            $imgName = $request->file('image')->getClientOriginalName();
            $data['image'] = $request->file('image')->storeAs(
                'assets/gallery/avatar',
                $imgName,
                'public'
            );

            User::where('id', $id)->update([
                'name' =>   $request['name'],
                'username' => $request['username'],
                'ktp' => $request['ktp'],
                'address' => $request['address'],
                'phone' => $request['phone'],
                'sim' => $request['sim'],
                'image' => $data['image']

            ]);
        } else {
            User::where('id', $id)->update([
                'name' =>   $request['name'],
                'username' => $request['username'],
                'ktp' => $request['ktp'],
                'address' => $request['address'],
                'phone' => $request['phone'],
                'sim' => $request['sim']
            ]);
        }


        return redirect()->back();
    }

    public function index()
    {
        $profile = User::where('id', Auth::id())->get();
        return view('pages.profile', compact('profile'));
    }
}
